
<?php 
    session_start();
    require_once '../configuration/config.php'; // ajout connexion bdd 
   // si la session existe pas soit si l'on est pas connecté on redirige
    if(!isset($_SESSION['user'])){
        header('Location:../connection/connection.php');
       
        die();
    }

    // On récupere les données de l'utilisateur
    $req = $bdd->prepare('SELECT * FROM users WHERE token = ?');
    $req->execute(array($_SESSION['user']));
    $data = $req->fetch();
   
?>
<!DOCTYPE html>
<html lang="fr">
<head>
<title>Vote Ping</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../nav_bar/nav_bar.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
<link rel="stylesheet" href="../acceuil/acceuil.css">
<style>
* {
  box-sizing: border-box;

}

body {
  font-family: Arial, Helvetica, sans-serif;
  background-image :url(back.jpg);
  background-size: cover;
}

/* Float four columns side by side */
.column {
  float: left;
  width: 25%;
  padding: 0 10px;
}

/* Remove extra left and right margins, due to padding */
.row {margin: 0 -5px;}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive columns */
@media screen and (max-width: 600px) {
  .column {
    width: 100%;
    display: block;
    margin-bottom: 20px;
  }
}

/* Style the counter cards */
.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  padding: 16px;
  text-align: center;
  border-radius:20px ;
  background-color: #085a6e;
  color: white;
}
</style>
</head>

<body>
<?php include("../exit_nav_bar/exit_nav_bar.php");
  if($data['hasvote']!=1)
  {
?>  

<h2> Faites ton choix!<?php echo $data['name'];?>!!</h2>
<p>Votez!!!!</p>
<?php
  }
  else {

    ?>
    <h2> Génial <?php echo $data['name'];?> Tu as déja voté!!</h2>   
    <?php
  }
   ?>

                   <?php 
                        require_once '../configuration/config.php';
                   
                    $reponse = $bdd->query('SELECT id, name, description , img_path  FROM ping');

                    // On affiche chaque entrée une à une
                    while ($donnees = $reponse->fetch())
                    {

                    ?>
                   

                    <div class="row">
                    <div class="column">
                      <div class="card">
                        <h3><?php echo $donnees['name']; ?></h3>
                        <p><?php echo $donnees['description']; ?></p>
                        <div> <img src="../admin/add_Poster/<?php echo $donnees['img_path'];?>"></div>     
                      </div>
                    </div>
                    <?php

                    if($data['hasvote']!=1)
                    {  ?>
                    <form  action="vote_traitement.php" method="post" >
                    <input type="hidden" name='vote' required="required" value="<?php echo $donnees['id']; ?>" >
                    <input type="submit" value="Vote">
                    </form>
                    <?php  }  
                    
                    }
                    $reponse->closeCursor(); // Termine le traitement de la requête    
                               
                        ?>



</body>
</html>