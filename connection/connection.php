
<!DOCTYPE html>
<html lang="fr">
    <title>Immune_app_web</title>
    <link rel="stylesheet" href="connection.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
      <meta name="viewport" content="width=device-width,initial-scale=1.0, user-scalable=0">
      <link rel="stylesheet" href="../nav_bar/nav_bar.css">
	  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
   </head>
   <body>
	   

   
   <?php include("../nav_bar/nav_bar.php"); ?>
<div class="cont">
   <?php 
                if(isset($_GET['login_err']))
                {
                    $err = htmlspecialchars($_GET['login_err']);

                    switch($err)
                    {
                        case 'password':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> mot de passe incorrect
                            </div>
                        <?php
                        break;

                        case 'email':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> email incorrect
                            </div>
                        <?php
                        break;

                        case 'already':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> compte non existant                               
                            </div>
                        <?php
                        break;
                    }
                }
                ?> 

		<div class="box">
			<h2>Login</h2>
			<form  action="connection_traitement.php" method="POST">
				<div class="inputBox">
					<input type="email" name= "email" required="required" autocomplete="off" >
					<label>Email</label>
				</div>
				<div class="inputBox">
					<input type="Password"  name= "password" required="required" autocomplete="off">
					<label>Password</label>
				</div>
				<input type="submit"  value="Submit">
			</form>
		</div>
        <a class="btn btn-primary" href="connection_badje.php"       >Connection avec votre Badje</a>
        </div>
   </body>
</html>
