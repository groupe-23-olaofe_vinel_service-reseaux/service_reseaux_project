

<?php 
 session_start();
 require_once '../../configuration/config.php'; // ajout connexion bdd 


if(!empty($_FILES)){
    $file_name = htmlspecialchars($_FILES['image']['name']);
    $file_extension = strrchr($file_name, ".");

    $file_tmp_name =htmlspecialchars($_FILES['image']['tmp_name']);
    $file_dest = 'Img/'.$file_name;

    $extension_autorisees = array('.png', '.PNG', '.jpg', '.JPG', '.jpeg', '.JPEG', '.svg', '.SVG', '.gif', '.GIF', 'webp','WEBP');

    if(in_array($file_extension,$extension_autorisees)){
        // On vérifie si l'utilisateur existe
        $check = $bdd->prepare('SELECT * FROM poster WHERE name = ?');
        $check->execute(array($_POST['name']));
        $data = $check->fetch();
        $row = $check->rowCount();

        if($row == 0)
        { 
            if(move_uploaded_file($file_tmp_name,$file_dest))
            {
                if(!empty($_POST['name']) && !empty($_POST['description']))
                {
                    // On insère dans la base de données
                    $nb_vote=0;
                    $insert = $bdd->prepare('INSERT INTO ping(name, description, img_path, nb_vote) VALUES( :name, :description , :img_path  , :nb_vote)');
                    $insert->execute(array(         
                        'name' => htmlspecialchars($_POST['name']),
                        'description' => htmlspecialchars($_POST['description']),
                        'img_path' => $file_dest,
                        'nb_vote' => $nb_vote                                                    
                    ));

                header('Location: add_Poster.php?reg_err=yes_upload'); die();
                }else{header('Location: add_Poster.php?reg_err=not_send'); die();}
            }else{header('Location: add_Poster.php?reg_err=not_upload'); die();}
        }else{header('Location: add_Poster.php?reg_err=already'); die();}
    }else{header('Location: add_Poster.php?reg_err=not_ext'); die();}
}else{header('Location: add_Poster.php?reg_err=not_img'); die();}

    
   
?>














        