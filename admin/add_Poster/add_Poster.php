
<!DOCTYPE html>
<html lang="fr">
    <title> Manage Ping vote</title>
    <link rel="stylesheet" href="add_Poster.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
	  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="../../nav_bar/nav_bar.css">
   </head>
   <body>
	   
   <?php include("../../exit_nav_bar/exit_nav_bar.php"); ?> 
<div class="cont">
   <?php 
                if(isset($_GET['reg_err']))
                {
                    $err = htmlspecialchars($_GET['reg_err']);
           
                    switch($err)
                    {
                        case 'yes_upload':
                        ?>
                            <div class="alert alert-success">
                                <strong>Succès</strong> ajout réussie !
                            </div>
                        <?php
                        break;

                      

                        case 'already':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> Ce projet exite déja
                            </div>
                        <?php 
                        break;

                       
                        case 'not_ext':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> Mauvaise extension pour l'image
                            </div>
                        <?php 
                        break;

                        case 'not_upload':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> Le fichier n'as pas put être téléchargé
                            </div>
                        <?php 
                        break;

                        case 'not_send':
                            ?>
                                <div class="alert alert-danger">
                                    <strong>Erreur</strong> Remplisser tous les champs
                                </div>
                            <?php 
                            break;
                    }
                }
                ?>

		<div class="box">
			<h2>Add a Poster</h2>
			<form  action="add_Poster_traitement.php" method="post" enctype="multipart/form-data">
				<div class="inputBox">
					<input type="text" name='name' required="required" autocomplete="off" >
					<label>Name</label>
				</div>
                <div class="inputBox">
					<input type="description" name="description"  required="required" autocomplete="off"  >
					<label>Description</label>
				</div>
				<div class="inputBox" >
					<input type="file" name="image"  autocomplete="off">
					<label>Image</label>
				</div>

				</div>
				<input type="submit" value="Create">
			</form>
		</div>
        </div>
   </body>
</html>
