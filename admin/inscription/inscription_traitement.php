<?php 
    require_once '../../configuration/config.php'; // On inclu la connexion à la bdd
 
    // Si les variables existent et qu'elles ne sont pas vides
    if(!empty($_POST['pseudo']) && !empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['id_statut']))
    {
       
        // Patch XSS
        $name = htmlspecialchars($_POST['pseudo']);
        $email = htmlspecialchars($_POST['email']);
        $num_rfid = htmlspecialchars($_POST['num_rfid']);
        $id_statut = htmlspecialchars($_POST['id_statut']);
        $password = htmlspecialchars($_POST['password']);

        // On vérifie si l'utilisateur existe
        $check = $bdd->prepare('SELECT name, email, num_rfid FROM users WHERE email = ?');
        $check->execute(array($email));
        $data = $check->fetch();
        $row = $check->rowCount();

        $email = strtolower($email); // on transforme toute les lettres majuscule en minuscule pour éviter que Foo@gmail.com et foo@gmail.com soient deux compte différents ..
                                                           
        // Si la requete renvoie un 0 alors l'utilisateur n'existe pas 
        if($row == 0){ 
            if(strlen($name) <= 100){ // On verifie que la longueur du pseudo <= 100
                if(strlen($email) <= 100){ // On verifie que la longueur du mail <= 100
                 
                    if(filter_var($email, FILTER_VALIDATE_EMAIL)){ // Si l'email est de la bonne forme                   
                           
                         
                        echo $name; echo $email; echo $num_rfid; echo $id_statut; echo $password; 
                            $id= 0;
                            $hasvote=0;
                             // On insère dans la base de données
                           
                             $insert = $bdd->prepare('INSERT INTO users( name, password, hasvote, id_statut, num_rfid, email, token) VALUES( ?, ?, ?, ?, ?, ?, ?)');

                             $insert->execute(array(                                
                                  $name,                                 
                                crypt($password, '$2a$10$1qAz2wSx3eDc4rFv5tGb5t'),
                                  $hasvote,
                                $id_statut,
                                 $num_rfid,
                                  $email,
                                 bin2hex(openssl_random_pseudo_bytes(64))
                             ));
                             echo $name; echo $email; echo $num_rfid; echo $id_statut; echo $password; 
                            // On redirige avec le message de succès
                            header('Location:inscription.php?reg_err=success');
                            die();
                       
                    }else{ header('Location: inscription.php?reg_err=email'); die();}
                }else{ header('Location: inscription.php?reg_err=email_length'); die();}
            }else{ header('Location: inscription.php?reg_err=pseudo_length'); die();}
        }else{ header('Location: inscription.php?reg_err=already'); die();}
    }

   
    
   
    ?>