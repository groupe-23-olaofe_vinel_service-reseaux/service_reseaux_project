
<!DOCTYPE html>
<html lang="fr">
    <title> Manage Ping vote</title>
    <link rel="stylesheet" href="inscription.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
	  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="../../nav_bar/nav_bar.css">
   </head>
   <body>
	   
   <?php include("../../exit_nav_bar/exit_nav_bar.php"); ?> 
<div class="cont">
   <?php 
                if(isset($_GET['reg_err']))
                {
                    $err = htmlspecialchars($_GET['reg_err']);
           
                    switch($err)
                    {
                        case 'success':
                        ?>
                            <div class="alert alert-success">
                                <strong>Succès</strong> inscription réussie !
                            </div>
                        <?php
                        break;

                        case 'password':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> mot de passe différent
                            </div>
                        <?php
                        break;

                        case 'email':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> email non valide
                            </div>
                        <?php
                        break;

                        case 'email_length':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> email trop long
                            </div>
                        <?php 
                        break;

                        case 'pseudo_length':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> pseudo trop long
                            </div>
                        <?php 
                        case 'already':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> compte deja existant
                            </div>
                        <?php 

                    }
                }
                ?>

		<div class="box">
			<h2>Add a user</h2>
			<form  action="inscription_traitement.php" method="post">
				<div class="inputBox">
					<input type="text" name='pseudo' required="required" autocomplete="off" >
					<label>Pseudo</label>
				</div>
                <div class="inputBox">
					<input type="email" name="email"  required="required" autocomplete="off"  >
					<label>Mail</label>
				</div>
				<div class="inputBox" >
					<input type="Password" name="password"   required="required" autocomplete="off">
					<label>Password</label>
				</div>
               
                <div class="inputBox">
					<input type="text" name="num_rfid"   required="required" autocomplete="off">
					<label>Num Bagde</label>
				</div>
                
                <div class="inputBox">
                    <select name="id_statut"  autocomplete="off" >
                        <?php 

                        // ajout connexion bdd 
                    // On récupère tout le contenu de la table jeux_video
                    require_once '../../configuration/config.php';
                    
                    $reponse = $bdd->query('SELECT id, statut_name FROM statut');

                    // On affiche chaque entrée une à une
                    while ($donnees = $reponse->fetch())
                    {
                    ?>
                        <option style="color:black;" value="<?php echo $donnees['id']; ?>"> <?php echo $donnees['statut_name']; ?> </option>
                    <?php
                    }
                    $reponse->closeCursor(); // Termine le traitement de la requête                   
                        ?>

                   </select>
				</div>
				<input type="submit" value="Submit">
			</form>
		</div>
        </div>
   </body>
</html>
