
<?php 
    session_start();
    require_once '../../configuration/config.php'; // ajout connexion bdd 
   // si la session existe pas soit si l'on est pas connecté on redirige
    if(!isset($_SESSION['user'])){
        header('Location:../connection/connection.php');
       
        die();
    }

    // On récupere les données de l'utilisateur
    $req = $bdd->prepare('SELECT * FROM users WHERE token = ?');
    $req->execute(array($_SESSION['user']));
    $data = $req->fetch();
   
?>
<!DOCTYPE html>
<html lang="fr">
<head>
<title>Vote Ping</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../nav_bar/nav_bar.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<link rel="stylesheet" href="../../acceuil/acceuil.css">
<style>
body {
  font-family: Arial, Helvetica, sans-serif;
  background-image :url(back.jpg);
  background-size: cover;
}
</style>
</head>

<body>
<?php include("../../exit_nav_bar/exit_nav_bar.php"); ?>

<?php 
                if(isset($_GET['reg_err']))
                {
                    $err = htmlspecialchars($_GET['reg_err']);
           
                    switch($err)
                    {
                        case 'yes_initialize':
                        ?>
                            <div class="alert alert-success">
                                <strong>Succès</strong> Réinitialisation réussie !
                            </div>
                        <?php
                    }
                }
                ?>

                    <form action="reinitialize_traitement.php" method="POST">                    
                    <input type="submit" class=" btn btn-danger deletebtn" value="Reinitialize"name="reinitialize">
                    </form>
</body>
</html>